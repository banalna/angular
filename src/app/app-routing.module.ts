import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PictureEditComponent } from './picture-edit/picture-edit.component';
import { FocusedPicturesComponent } from './focused-pictures/focused-pictures.component';
import {GanerateCollagesComponent} from './ganerate-collages/ganerate-collages.component';

import { AppComponent } from './app.component';

const routes: Routes = [
  {path: 'Add-avatars', component: PictureEditComponent},
  {path: 'Focused-pictures', component: FocusedPicturesComponent},
  {path: 'Ganerate-collages', component: GanerateCollagesComponent},
  {path: '**', component: PictureEditComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
