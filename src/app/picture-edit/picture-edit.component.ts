import { Component, OnInit, Inject } from '@angular/core';

import {Source} from '../source-list/source';

@Component({
  selector: 'picture-edit',
  templateUrl: './picture-edit.component.html',
  styleUrls: ['./picture-edit.component.scss']
})
export class PictureEditComponent implements OnInit {
  
  private sources: string[];

  constructor(private source:Source) {
    this.sources = source.sources;
 }

  ngOnInit() {
  }


public onImageLoad(results) {
  console.log('Image loaded: ', results);
  this.source.sources.push(results.img.url);
}

public onImageDelete(results) {
  setTimeout(() =>{

  let elements = document.getElementsByTagName('pip-picture-edit')
  let src:string[]=[];

  for(let i=0; i<elements.length; i++) {
    let el = elements[i].getElementsByTagName('img')[0].getAttribute('src');
    if (el != "null"){
      src.push(el);
    }
  }

  this.source.sources = src;
  console.log('Image deleted!',src);
  }, 100);
}

}
