import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCardModule} from '@angular/material/card';

import {PipPictureEditModule, PipAddImageModule} from 'pip-webui2-pictures';

import {PictureEditComponent} from './picture-edit.component';

@NgModule({
  declarations: [PictureEditComponent],
  imports: [
    CommonModule,
    MatCardModule,
    
    PipPictureEditModule,
    PipAddImageModule,
  ],
})
export class PictureEditModule { }
