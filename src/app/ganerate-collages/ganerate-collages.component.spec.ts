import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GanerateCollagesComponent } from './ganerate-collages.component';

describe('GanerateCollagesComponent', () => {
  let component: GanerateCollagesComponent;
  let fixture: ComponentFixture<GanerateCollagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GanerateCollagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GanerateCollagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
