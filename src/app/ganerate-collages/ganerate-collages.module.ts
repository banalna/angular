import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GanerateCollagesComponent } from './ganerate-collages.component';
import {MatCardModule} from '@angular/material/card';

import {PipPictureListEditModule} from 'pip-webui2-pictures';
import {PipCollageModule} from 'pip-webui2-pictures';
@NgModule({
  declarations: [GanerateCollagesComponent],
  imports: [
    CommonModule,
    MatCardModule,

    PipPictureListEditModule,
    PipCollageModule,
  ],
})
export class GanerateCollagesModule { }
