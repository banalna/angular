import { Component, OnInit } from '@angular/core';

import {Source} from '../source-list/source';

@Component({
  selector: 'app-focused-pictures',
  templateUrl: './focused-pictures.component.html',
  styleUrls: ['./focused-pictures.component.scss']
})
export class FocusedPicturesComponent implements OnInit {

  private sources: string[];

  constructor(private source:Source,) {
    this.sources = source.sources;
 }

  ngOnInit() {
  }

}
