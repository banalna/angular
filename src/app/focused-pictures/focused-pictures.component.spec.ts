import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FocusedPicturesComponent } from './focused-pictures.component';

describe('FocusedPicturesComponent', () => {
  let component: FocusedPicturesComponent;
  let fixture: ComponentFixture<FocusedPicturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FocusedPicturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FocusedPicturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
