import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TranslateModule } from '@ngx-translate/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {PictureEditModule} from './picture-edit/picture-edit.module';

import {Source} from './source-list/source';
import {GanerateCollagesModule} from './ganerate-collages/ganerate-collages.module';
import { FocusedPicturesModule } from './focused-pictures/focused-pictures.module';


@NgModule({
  declarations: [
    AppComponent,
  ],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot(),

    AppRoutingModule,
    PictureEditModule,
    GanerateCollagesModule,
    FocusedPicturesModule
    
  ],
  providers: [Source],
  bootstrap: [AppComponent]
})
export class AppModule { }
